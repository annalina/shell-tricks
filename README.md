# shell tricks

collection of scripts or just simple instructions I followed for fixing things with my shell.


# Linux

## How to set up colors  
The variable responsible for the colors is `LS_COLORS`. Type `echo $LS_COLORS` to check if the colors have been set up or not.

## How to add permanently a key
- edit or create a file `config` in `~/.ssh`
- add for each key the entry `IdentityFile ~/.ssh/key_name`. Watch out: you don't need to add the extension `.pub`.


# Apple

## How to set up colors for 'ls' in the shell

1. Following this blog:  I found out that the parameter `G` of `ls` actually print outs the colors. So, I've added the following command:  
`alias ls="ls -G"` to the file `~/.bashrc`. You have two options here actually:  
- add the command to the file  `~/.bashrc`: this will be executed once for all the shells;  
- add it to `~/.bash_profile`: this will be executed for every shell;  
Also, you can add other options to the `ls` command, like `ls -GFlash`, which will print the dimension in a pretty way etc.  

2. A more neat solution is to work directly with the equivalent of LS_COLORS in OS X. Again, I've worked with the file `~.bashrc` and added the following lines of code:  
`export CLICOLOR=1` 
`export LSCOLORS=GxFxCxDxBxegedabagaced`

The first line set the coloring in the termina, while the `LSCOLORS` specify the pattern to be used for each attribute, as follows:  
- a black
- b red
- c green
- d brown
- e blue
- f magenta
- g cyan
- h light grey
- A bold black
- B bold red
- C bold green
- D bold brown
- E bold blue
- F bold magenta
- G bold cyan
- H bold light grey
- x default foreground or background

With the following order:

1. directory
2. symbolic link
3. socket
4. pipe
5. executable
6. block special
7. character special
8. executable with setuid bit set
9. executable with setgid bit set
10. directory writable to others, with sticky bit
11. directory writable to others, without sticky